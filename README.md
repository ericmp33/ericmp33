## Hello World! <img alt="hello gif" src="https://raw.githubusercontent.com/iampavangandhi/iampavangandhi/master/gifs/Hi.gif" width="24px">

--------------------------------------------------------------------

-  **Studying :** Full-Stack Web Development🖥️👨‍💻
-  **Fun fact :** Don't use light themes!🤢☢️
-  **Contact :** contactwEric@gmail.com📧📮

--------------------------------------------------------------------

### Tech Stack🛠

![Java](https://img.shields.io/badge/-Java-333333?style=flat&logo=Java&logoColor=EA891C)
![JavaScript](https://img.shields.io/badge/-JavaScript-333333?style=flat&logo=javascript)
![HTML5](https://img.shields.io/badge/-HTML5-333333?style=flat&logo=HTML5)
![CSS](https://img.shields.io/badge/-CSS-333333?style=flat&logo=CSS3&logoColor=1572B6)
![MySQL](https://img.shields.io/badge/-MySQL-333333?style=flat&logo=mysql)
![Git](https://img.shields.io/badge/-Git-333333?style=flat&logo=git)
![GitHub](https://img.shields.io/badge/-GitHub-333333?style=flat&logo=github)
![JetBrains](https://img.shields.io/badge/-JetBrains-333333?style=flat&logo=jetbrains)
![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-333333?style=flat&logo=visual-studio-code&logoColor=007ACC)

--------------------------------------------------------------------

### I speak...🔤

Catalan & English

<!-- <div
    style="
        width: 490px;
        height: 190px;
        overflow: hidden;
        border-radius: 10px;
        border: 1.2px solid #7490ac;
    "
>
    <img
        style="margin: -1px 0px 0px -1px"
        alt="ericmp33 stats"
        src="https://github-readme-stats.vercel.app/api?username=ericmp33&show_icons=true&theme=nord"
    />
</div> -->
